# LIBRARY IMPORT
from io import BytesIO
import os

import pandas as pd
import json
import csv

import gzip
import zipfile
import lzma
import tarfile

import numpy as np
import matplotlib.pyplot as plt

# FUNCTIONS FOR LOADING A FILE

def get_file_format(file_name:str):
    '''
    get the file format of the file
    '''
    _, file_format = os.path.splitext(file_name)
    return file_format

def load_file(file_path:str):
    '''
    Load a file into a pandas dataframe
    /!\ Careful, it dont handle compressed file
    '''
    file_format = get_file_format(file_path)
    match file_format:
        case "csv":
            return load_csv_file(file_path)
        case "json":
            return load_json_file(file_path)
        case _:
            return None
    
def load_csv_file(filepath):
    '''
    Load a CSV file into a pandas dataframe
    '''
    try:
        df = pd.read_csv(filepath)
    except Exception:
        df = None
    return df

def load_json_file(filepath):
    '''
    Load a JSON file into a pandas dataframe
    '''
    try:
        df = pd.read_json(filepath)
    except Exception as e:
        df = None
    return df

# FUNCTION FOR HANDLING COMPRESSED FILE

def compress_csv(df, file_path, archive_format):
    df.to_csv(file_path, index=False, compression=archive_format)

def compress_json(df, file_path, archive_format):
    df.to_json(file_path, index=False, compression=archive_format)

def uncompress(filepath:str):
    '''
    uncompressed a file into an IO stream
    '''
    file_format = filepath.split(".")[-1]
    match file_format:
        case "zip":
            return uncompress_zip_file(filepath)
        case "xz":
            return uncompress_xz_file(filepath)
        case _:
            return None

def uncompress_zip_file(filepath):
    '''
    uncompressed a ZIP file into a ByteIO
    '''
    with zipfile.ZipFile(filepath, 'r') as files:
        filename = files.namelist()[0]
        file = files.open(filename)
    return file
     
def uncompress_xz_file(filepath):
    '''
    uncompressed an XZ file into a TextIO
    '''
    file = lzma.open(filepath, mode='rt', encoding='utf-8')
    return file

# FUNCTIONS FOR CREATING CHART
def create_line_chart(x:int, y:int, **kwargs):
    '''
    Create a line chart 
    '''
    plt.plot(x, y, label='sin(x)', color='blue', linestyle='--')
    plt.title(kwargs['title'])   if 'title'  in kwargs.keys() else None
    plt.xlabel(kwargs['xlabel']) if 'xlabel' in kwargs.keys() else None
    plt.ylabel(kwargs['ylabel']) if 'ylabel' in kwargs.keys() else None
    plt.legend()
    plt.grid(True)
    plt.show()

def create_bar_chart(x:str, y:int, **kwargs):
    '''
    Create a bar chart 
    '''
    plt.bar(x, y, align='center', alpha=0.5)
    plt.xticks(x, y, rotation=30)
    plt.title(kwargs['title'])   if 'title'  in kwargs.keys() else None
    plt.xlabel(kwargs['xlabel']) if 'xlabel' in kwargs.keys() else None
    plt.ylabel(kwargs['ylabel']) if 'ylabel' in kwargs.keys() else None
    plt.show()

def create_pie_chart(x:str, y:int, **kwargs):
    '''
    Create a pie chart 
    '''
    plt.pie(y,labels=x)
    plt.title(kwargs['title'])   if 'title'  in kwargs.keys() else None
    plt.xlabel(kwargs['xlabel']) if 'xlabel' in kwargs.keys() else None
    plt.ylabel(kwargs['ylabel']) if 'ylabel' in kwargs.keys() else None
    plt.legend()
    plt.show()


