from models import Fruit

class Banane(Fruit):
    def __init__(self, _poids, _color):
        super().__init__(self, _poids)
        self.color = _color
    
    # Définition des méthodes
    def get_poids(self):
        return super().get_poids(self)
        
